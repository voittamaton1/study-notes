== HTTP methods

Requesting methods in HTTP indicate the action executed over a particular resource.

There exist nine methods. PATCH is specified in RFC 5789, others are specified in RFC 7231.

[cols="5,25,5,5,5"]
|===
^|Method
^|Purpose
^|Safe
^|Idempotent
^|Cacheability
|OPTIONS
|To describe resources: the supported HTTP methods, options, requirements, and parameters.
^|Yes
^|Yes
^|No
|HEAD
|To returns only the meta information of HTTP headers of a GET method. Body content of an entity isn't provided.
^|Yes
^|Yes
^|Yes
|TRACE
|To return the entity request to the client. Typically, is used by gateways and proxies.
^|Yes
^|Yes
^|No
|CONNECT
|To establish connections with SSL-enabled websites.
^|No
^|No
^|No
|GET
|To return information about a resource. Can both return already available data or trigger a data-producing process in the server.
^|Yes
^|Yes
^|Yes
|POST
|To save a new entity of a resource.
^|No
^|No
^|Only if freshness information is included
|PUT
|To update an existing entity of a resource. If the entity doesn't exist, the server creates a new entity.
^|No
^|Yes
^|No
|PATCH
|To update only particular portions of an entity of a resource.
^|No
^|No
^|No
|DELETE
|To delete an entity of a resource.
^|No
^|Yes
^|No
|===

== Properties

Properties of HTTP methods are:

* Safety. A method is safe if it only retrieves information.
* Idempotency. The intended effect on the server of multiple identical requests is the same as the effect for single request. The idempotency property only applies to what has been requested by the user. The server is free to cause some side effects: logging, monitoring, etc.
* Cacheability. Responses are allowed to be stored for future reuse.

== Extra information

=== Why PUT is idempotent and PATCH is not?

The PATCH method has more use cases than the PUT method. Some use cases are not idempotent, such as appending text lines or rows to database tables.
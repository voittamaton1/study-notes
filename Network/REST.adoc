== Description

REST (Representational state transfer) is a software architectural style that describes a uniform interface between decoupled components in the Internet and client-server architecture.

== REST interface constraints

* Identification of resources
* Manipulation of resources
* Self-descriptive messages
* Hypermedia as the engine of application state

== Architectural constraints

* Client-server architecture
* Statelessness
* Cacheability
* Layered system
* Code on demand (optional)
* Uniform interface
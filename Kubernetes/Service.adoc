Exposes a network application that is running as one or more Pods in a cluster.

The set of Pods targeted by a Services is usually determined by a selector.
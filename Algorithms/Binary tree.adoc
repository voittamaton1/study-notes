== Description

A Binary tree is a hierarchical data structure in which each node has at most two children. The child nodes are called the left child and the right child.

Internal nodes are nodes except leaves.

Each node has three fields:

* Pointer to store the address of the left child.
* Data element.
* Pointer to store the address of the right child.

Properties:

* A Binary tree can have a maximum elements 2^L at level L, if the level of the root is zero.
* There exists a maximum of 2^H-1 nodes in a Binary tree of a height H. Height of a tree with a single node is considered as 1.
** If a height is 4 (including the root note), then there are `2^4 - 1 = 16 - 1 = 15` nodes (level 0 - 1 node, level 1 - 2 nodes, level 2 - 4 nodes, level 3 - 8 nodes).
* In a Binary tree with N nodes, minimum number of levels is log2(N+1) (round up).
** If there are 14 nodes (level 0 - 1 node, level 1 - 2 nodes, level 2 - 4 nodes, level 3 - 7 nodes), then minimum amount of levels is `roundUp(log2(15)) = roundUp(3.9) = 4`. Amount of levels could be 5 or more, but not less, than 3.
* If there exist L leaf nodes in a Binary tree, then it has at least log2(L)+1 levels.
** If there are 8 leaves, then there are `log2(8) + 1 = 4` levels.
* The number of leaf nodes is one more that the number of nodes that have two children.
* A Binary three with N nodes has N+1 null references.

== Types

=== Full Binary tree

* Every node has 0 or 2 children.

=== Complete Binary tree

* All levels are completely filled, except the last level.
* On the last level all nodes must be located from left to right.

=== Perfect Binary tree

* All the internal nodes have strictly two children.
* All the leaf nodes are at the same level.

=== Balanced Binary tree

* Difference of height between the left and right subtrees for every node is 1.

=== Degenerate or Pathological tree

* Each internal node has a single child.

=== Skewed Binary tree

* Each internal node has a single child.
* All children are left or all children are right.
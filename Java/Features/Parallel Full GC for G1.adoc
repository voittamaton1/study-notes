 Java 10.
 JEP 307.

== Summary

Improves G1 worst-case latencies by making the full GC parallel.

== Motivation

The G1 was made the default in JDK 9. The previous default, the parallel collector, has a parallel full GC. To minimize the impact for users experiencing full GCs, the G1 full GC should be made parallel as well.
== Description

Class loaders are responsible for loading Java classes dynamically to the JVM during runtime. They are part of the JRE, therefore, the JVM doesn't need to know about the underlying files in order to run Java programs.

Java classes aren't loaded into memory all at once, but rather when they're required by an application.

There are several types of class loaders:

* Bootstrap class loader
* Extension class loader
* System (application) class loader
* Custom class loader

=== Bootstrap class loader

The bootstrap class loader serves as the parent of all the other ClassLoader instances.

It is part of the core JVM and is written in native code.

Loads JDK internal classes, typically rt.jar and other core libraries.

=== Extension class loader

The extension class loader takes care of loading the extensions of the standard core Java classes.

It is a child of the bootstrap class loader.

Loads from the JDK extensions directory, or any other directory mentioned in the java.ext.dirs system property.

=== System (application) class loader

The system or application class loader takes care of loading all the application level classes.

It is a child of the extension class loader.

Loads from paths in the classpath environment variable.

=== Custom class loader

The build-in class loader is sufficient for most cases where the files are already in the file system.

Custom class loaders use cases:

* Modify the existing bytecode, e.g. weaving agents.
* Create classes dynamically suited to the user's needs, e.g., in JDBC, switching between different driver implementations is done through dynamic class loading.
* Implement a class versioning mechanism while loading different bytecodes for classes with the same names and packages. This can be done either through a URL class loader (load jars via URLs) or custom class loaders.

== How do class loaders work

* When the JVM requests a class, the class loader tries to locate the class and load the class definition into the runtime using the fully qualified class name. The ClassLoader#loadClass method is responsible for it.
* If the class isn't already loaded, the class loader delegates the request to the parent class loader.
* If the parent class loader doesn't find the class, then the child class will call the URLClassLoader#findClass method to look for classes in the file system itself.
* If the last child class loader isn't able to load the class, it throws NoClassDefFoundError or ClassNotFoundException.
== Reserved keywords

* Primitive types: byte, short, int, long, float, double, char, boolean.
* Cycles: if, else, switch, case, default, while, do, for, break, continue, yield.
* Exceptions: try, catch, finally, throw, throws.
* Access modifiers: private, protected, public.
* Declaration, import: import, package, static, final, void, native, class, interface, abstract, extends, implements.
* Creation, return, call: new, return, this, super.
* Concurrency: synchronized, volatile.
* Miscellaneous: instanceof, enum, assert, transient, strictfp.
* Unused: const, goto.
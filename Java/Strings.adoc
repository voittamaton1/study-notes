== Description

String is one of the most used classes in Java. It is represented internally by byte[] (before Java 9 - char[]).

Before and up to Java 8 Strings are stored in the UTF-16 format internally.

Java 9 introduced compact Strings:

* If a String contains only 1-byte characters, it will be represented using Latin-1 encoding.
* If a String contains at least 1 multibyte character, it will be represented as 2 bytes per character using UTF-16 encoding.

A String is a derived type, because it has state and behaviour.

But as it is a very important type, it has some special characteristics:

* They are stored in a special memory region String pool.
* Operator "+" can be used on strings, like for primitives.
* An instance can be created without the "new" keyword, like for primitives.

A String is immutable. It brings benefits:

* The String pool is only possible if the string are never changed. So they can be reused.
* A String can be safely passed to a method with a guarantee, that it will not be changed.
* A String is thread-safe. There is no need to synchronize common data. This improves performance.
* Hashcode can be easily cached.

The Locale class allows to differentiate between cultural locales and format content appropriately.

Without it, we can get problems with portability, security and usability.

When the char[] type is used to store passwords, it can be wiped. When the String type is used to store passwords, we can rely only on a Garbage collector.

== Memory

According to the JVM specification, String literals are stored in a runtime Constant pool.

The Constant pool is allocated from a Method area. The Method area is a logically part of a Heap memory. The JVM specification doesn't dictate a location, memory size and garbage collection policies. It can be implementation-specific.

The runtime Constant pool for a class or interface is constructed when the class or interface is created by the JVM.

Strings in the String pool are eligible for garbage collection.

The String pool (String constant pool, String intern pool) is a special memory region where the JVM stores String instances.

It optimizes an application performance:

* The JVM stores only one copy of a particular String in the pool.
* When creating a new String, the JVM searches in the pool for a String having the same value.
** If found, the JVM returns the reference to that String without allocating any additional memory.
** If not found, the JVM adds it to the pool (interns it) and returns its reference).

== StringBuilder, StringBuffer

StringBuilder is used to alter String.

StringBuffer is synchronized and thread-safe.
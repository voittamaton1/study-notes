== Casting

=== Upcasting

Upcasting (widening) - a casting from a subclass to a superclass.

Narrows a list of methods and fields.
This is implicit casting without brackets.

Use cases:

* To accommodate a smaller primitive type value in a larger primitive type.
* To accommodate reference variable of a subclass in reference variable of its superclass.

=== Downcasting

Downcasting (narrowing) - a casting from a superclass to a subclass.

This is explicit casting with brackets.

Use cases:

* To accommodate a larger primitive type value in a smaller primitive type.
* To accommodate reference variable of a superclass in reference variable of its subclass.

  Object object = new Object();
  String string = (String) object; // ClassCastException

  Object object = new String("");
  String string = (String) object; // Successful runtime

=== Common rules for primitives

* byte, short, char are cast to int in expressions.
* If an expression has double, float, long, then a result will be cast to a type, according to the following priority: double - float, long.
* If there will be a choice between a length and fraction, then the fraction will be chosen.
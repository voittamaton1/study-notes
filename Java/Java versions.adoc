== Java 21 (2023, September 19, LTS)

* xref:Features/Record Patterns.adoc[Record Patterns]
* Pattern Matching for switch
* String Templates (Preview)
* Unnamed Patterns and Variables (Preview)
* Unnamed Classes and Instance Main Methods (Preview)
* Virtual Threads
* Sequenced Collections
* Key Encapsulation Mechanism API
* Foreign Function & Memory API (Third Preview)
* Structured Concurrency (Preview)
* Scoped Values (Preview)
* Vector API (Sixth Incubator)
* Generational ZGC
* Prepare to Disallow the Dynamic Loading of Agents

== Java 20 (2023, March 21)

* Record Patterns (Second Preview)
* Pattern Matching for switch (Fourth Preview)
* Foreign Function & Memory API (Second Preview)
* Vector API (Fifth Incubator)
* Scoped Values (Incubator)
* Virtual Threads (Second Preview)
* Structured Concurrency (Second Incubator)

== Java 19 (2022, September 20)

* Virtual Threads (Preview)
* Structured Concurrency (Incubator)
* Record Patterns (Preview)
* Pattern Matching for switch (Third Preview)
* Foreign Function & Memory API (Preview)
* Vector API (Fourth Incubator)

== Java 18 (2022, March 22)

* UTF-8 by Default
* Simple Web Server
* Reimplement Core Reflection with Method Handles
* Internet-Address Resolution SPI
* Code Snippets in Java API Documentation
* xref:Features/Vector API.adoc[Vector API (Third Incubator)]
* Foreign Function & Memory API (Second Incubator)
* Pattern Matching for switch (Second Preview)

== Java 17 (2021, September 14, LTS)

* Restore Always-Strict Floating-Point Semantics
* Enhanced Pseudo-Random Number Generators
* New macOS Rendering Pipeline
* macOS/AArch64 Port
* Deprecate the Applet API for Removal
* Strongly Encapsulate JDK Internals
* Pattern Matching for switch (Preview)
* Remove RMI Activation
* Sealed Classes
* Remove the Experimental AOT and JIT Compiler
* Deprecate the Security Manager for Removal
* Foreign Function & Memory API (Incubator)
* xref:Features/Vector API.adoc[Vector API (Second Incubator)]
* Context-Specific Deserialization Filters

== Java 16 (2021, March 16)

* xref:Features/Vector API.adoc[Vector API (Incubator)]
* Enable C++14 Language Features
* Migrate from Mercurial to Git
* Migrate to GitHub
* ZGC: Concurrent Thread-Stack Processing
* Unix-Domain Socket Channels
* Alpine Linux Port
* Elastic Metaspace
* Windows/AArch64 Port
* Foreign Linker API (Incubator)
* Warnings for Value-Based Classes
* Packaging Tool
* Foreign-Memory Access API (Third Incubator)
* xref:Features/Pattern Matching for instanceof.adoc[Pattern Matching for instanceof]
* Records
* Strongly Encapsulate JDK Internals by Default
* Sealed Classes (Second Preview)

== Java 15 (2020, September 16)

* Sealed Classes and Interfaces (Preview)
* EdDSA Algorithm
* Hidden Classes
* Pattern Matching for instanceof (Second Preview)
* Removed Nashorn JavaScript Engine
* Reimplement the Legacy DatagramSocket API
* Records (Second Preview)
* Text Blocks become a standard feature

== Java 14 (2020, March 17)

* Pattern Matching for instanceof (Preview)
* Text Blocks (Second Preview)
* Helpful NullPointerExceptions
* Records (Preview)
* Switch Expressions (Standard)
* Packaging Tool (Incubator)
* NUMA-Aware Memory Allocation for G1
* JFR Event Streaming
* Non-Volatile Mapped Byte Buffers
* Remove the Concurrent Mark Sweep (CMS) Garbage Collector
* Remove the Pack200 Tools and API
* Foreign-Memory Access API (Incubator)

== Java 13 (2019, September 17)

* Text Blocks (Preview)
* Switch Expressions Enhancements (Preview)
* Reimplement the Legacy Socket API
* Dynamic CDS Archive
* ZGC: Uncommit Unused Memory
* FileSystems.newFileSystem() Method
* DOM and SAX Factories with Namespace Support

== Java 12 (2019, March 19)

* Collectors.teeing() in Stream API
* String API Changes
* Files.mismatch(Path, Path)
* Compact Number Formatting
* Support for Unicode 11
* Switch Expressions (Preview)

== Java 11 (2018, September 25, LTS)

* HTTP Client API
* Launch Single-File Programs Without Compilation
* String API Changes
* Collection.toArray(IntFunction)
* Files.readString() and Files.writeString()
* Optional.isEmpty()

== Java 10 (2018, March 20)

* xref:Features/Local Variable Type Inference.adoc[Local Variable Type Inference]
* Time-Based Release Versioning
* xref:Features/Garbage Collector Interface.adoc[Garbage-Collector Interface]
* xref:Features/Parallel Full GC for G1.adoc[Parallel Full GC for G1]
* Heap Allocation on Alternative Memory Devices
* Consolidate the JDK Forest into a Single Repository
* Application Class-Data Sharing
* Additional Unicode Language-Tag Extensions
* Root Certificates
* Experimental Java-Based JIT Compiler
* Thread-Local Handshakes
* Remove the Native-Header Generation Tool
* New Added APIs and Options
* Removed APIs and Options

== Java 9 (2017, September 21)

* Java platform module system
* Interface Private Methods
* HTTP 2 Client
* JShell - REPL Tool
* Platform and JVM Logging
* Process API Updates
* Collection API Updates
* Improvements in Stream API
* Multi-Release JAR Files
* @Deprecated Tag Changes
* Stack Walking
* Java Docs Updates
* Miscellaneous Other Features

== Java 8 (2014, March 18, LTS)

* Lambda expression support in APIs
* Stream API
* Functional interface and default methods
* Optionals
* Nashorn - JavaScript runtime which allows developers to embed JavaScript code within applications
* Annotation on Java Types
* Unsigned Integer Arithmetic
* Repeating annotations
* New Date and Time API
* Statically-linked JNI libraries
* Launch JavaFX applications from jar files
* Remove the permanent generation from GC
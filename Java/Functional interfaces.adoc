== Description

Functional interface - an interface with only 1 abstract method.

A functional interface can additionally have default and static methods.

@FunctionalInterface checks whether an interface is correct and shows error of a compilation. Usage of the annotation is optional.

Usage:

* In lambda expressions and method references.

== Best practices

* Use standard functional interfaces.
* Use @FunctionalInterface annotation.
* Don't overuse default methods in functional interfaces.
* Don't overload methods with functional interfaces as parameters.
* Instantiate functional interfaces with lambda expressions, but not with inner classes.
* Keep lambda expressions short and self-explanatory.

== Standard functional interfaces

Package java.util.function consist of the following functional interfaces:

* Supplier<T>, T get().
* Consumer<T>, void accept(T t).
* BiConsumer<T, U>, void accept(T t, U u).
* Predicate<T>, boolean test(T t).
* BiPredicate<T, U>, boolean test(T t, U u).
* Function<T, R>, R apply(T t).
* BiFunction<T, U, R>, R apply(T t, U u).
* UnaryOperator<T> extends Function<T, T>.
* BinaryOperator<T> extends BiFunction<T, T, T>.
* Functional interfaces for primitive types.

Package java.lang consist of the following functional interfaces:

* AutoClosable.
* Comparable.
* Iterable.
* Readable.
* Runnable.
== Docker vs Virtual machines

[cols="5,25,25"]
|===
^|Characteristic
^|Docker
^|Virtual machine
3+^|Definition and OS
|Definition
|Is a tool that uses containers to make creation, deployment, and running of application a lot easier.
|Is a system which acts exactly like a computer.
|Abstraction layer
|Runs on virtualization of the same operating system.
|Runs on the same hardware.
|Operating system support
|Doesn't have its own OS.
|Has its guest OS above the host OS.
3+^|Security
|Security
|Docker resources are shared and not namespaced. An attacker can exploit all containers in a cluster if he gets access to even one container.
|Strong isolation in the host kernel, because OS isn't shared.
3+^|Portability
|Portability
|Is easier because they don't have separate OS.
|Is more difficult because of a separate OS and size.
|Portability between hosts
|Containers are destroyed and recreated.
|VM can be moved.
3+^|Scalability
|Scalability
|Is easier.
|Is harder.
3+^|Performance
|Performance
|Starts faster.
|Starts longer.
|Overhead
|Is lower because containers share the host OS.
|Is higher because VMs use their own OS.
|Size
|Is less.
|Is larger.
|===
== Description

Horizontal partitioning, also known as horizontal scaling and scaling out.

Benefits:

* Not limited in terms of storage and computer power, unlike vertical partitioning.
* Speeding up queries.
* Mitigating the impact of outages.

Drawbacks:

* Complexity of properly implementing a sharded database architecture. If done incorrectly, there is a significant risk that the sharding process can lead to lost data.
* Rather than maintaining a single entry point, multiple shards should be maintained.
* Shards could become unbalanced.
* Complexity of returning to unsharded architecture.
* Isn't natively supported by every database engine.

== Sharding architectures

=== Key based sharding

Key based sharding, also known as hash based sharding, involves using a shard key and plugging in into a hash function to
determine which shard should be used.

Shard key should be taken from the particular column with static data. For instance, it could be primary key. If shard key is not static, then update operations could slow down performance.

When a new shard is added, other shards need rebalancing.

=== Range based sharding

Involves sharding data based on ranges of a given value.

=== Directory based sharding

Uses a lookup table with mapping of shard keys and shards.